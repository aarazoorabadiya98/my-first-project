import React from "react";
import ReactDOM from "react-dom";
import Routes from "./Routes";
import * as serviceWorker from "./serviceWorker";
import "../src/assets/commonStyles/style.scss";
import store from "./configStore";
import { loginSuccess } from "./actions";

let auth_token = localStorage.getItem("auth_Token");

auth_token && store.dispatch(loginSuccess({ authToken: auth_token }));

function render() {
  ReactDOM.render(<Routes store={store} />, document.getElementById("root"));
}

store.subscribe(render);
render();

serviceWorker.register();
