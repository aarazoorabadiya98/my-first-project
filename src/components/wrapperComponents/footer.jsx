import React from "react";

function Footer() {
  return (
    <footer className="footer text-center font-normal">
      2021 &copy; React App
    </footer>
  );
}
export default Footer;
