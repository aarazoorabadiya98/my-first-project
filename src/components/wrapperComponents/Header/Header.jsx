import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import logo from "../../../assets/logo192.png";
import defaultAvatar from "../../../assets/images/defaultAvatar.png";
import { LogoutApi } from "../../../actions";

function Header(props) {
  const logout = () => {
    //this.props.LogoutApi()
    localStorage.clear();
    props.history.push("/login");
  };

  return (
    <nav className="navbar navbar-default navbar-static-top m-b-0">
      <div className="navbar-header">
        <a
          className="navbar-toggle hidden-sm hidden-md hidden-lg"
          href="/"
          data-toggle="collapse"
          data-target=".navbar-collapse"
        >
          <i className="ti-menu"></i>
        </a>
        <div className="top-left-part">
          <NavLink to="/" className="logo">
            <b>
              <img src={logo} alt="home" className="headerLogo" />
            </b>
            <span className="hidden-xs">
              <b>React</b>
            </span>
          </NavLink>
        </div>

        <ul className="nav navbar-top-links navbar-left hidden-xs">
          <li>
            <NavLink
              to="#"
              className="open-close hidden-xs waves-effect waves-light"
            >
              <i className="icon-arrow-left-circle ti-menu"></i>
            </NavLink>
          </li>
        </ul>
        <ul className="nav navbar-top-links navbar-right pull-right">
          <li className="dropdown">
            <NavLink
              to="#"
              className="dropdown-toggle profile-pic"
              data-toggle="dropdown"
            >
              <img
                src={defaultAvatar}
                alt="user"
                width="36"
                className="img-circle"
                onError={(e) => {
                  e.target.onerror = null;
                  e.target.src = defaultAvatar;
                }}
              />
              <b className="hidden-xs">Admin</b>
            </NavLink>
            <ul className="dropdown-menu dropdown-user animated flipInY">
              <li role="separator" className="divider"></li>
              <li>
                <NavLink to="/login" onClick={() => logout()}>
                  <i className="fa fa-power-off"></i> Logout
                </NavLink>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  );
}
const mapStateToProps = (state) => ({
  user: state.auth.user,
});
export default connect(
  mapStateToProps,

  { LogoutApi }
)(Header);
