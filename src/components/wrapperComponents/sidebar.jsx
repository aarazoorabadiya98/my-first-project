import React from "react";
import { NavLink } from "react-router-dom";
import defaultAvatar from "../../assets/images/defaultAvatar.png";
import { LogoutApi } from "../../actions";
import { connect } from "react-redux";

function Sidebar(props) {
  const logout = () => {
    //this.props.LogoutApi()
    localStorage.clear();
    this.props.history.push("/login");
  };

  return (
    <div className="navbar-default sidebar" role="navigation">
      <div className="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul className="nav" id="side-menu">
          <li className="sidebar-search hidden-sm hidden-md hidden-lg"></li>
          <li className="user-pro">
            <a href="# " className="waves-effect">
              <img
                src={defaultAvatar}
                alt="user"
                className="img-circle"
                onError={(e) => {
                  e.target.onerror = null;
                  e.target.src = defaultAvatar;
                }}
              />
              <span className="hide-menu">
                Admin
                <span className="fa arrow"></span>
              </span>
            </a>
            <ul className="nav nav-second-level">
              <li>
                <NavLink to="/login" onClick={() => logout()}>
                  <i className="fa fa-power-off"></i> Logout
                </NavLink>
              </li>
            </ul>
          </li>
          <li className="nav-small-cap m-t-10">--- Main Menu</li>

          <li>
            <NavLink to="/dashboard" className="waves-effect">
              <i className="fa fa-dashboard fa-fw"></i>
              <span className="hide-menu"> Dashboard</span>
            </NavLink>
          </li>

          <li>
            <NavLink to="/users" className="waves-effect">
              <i className="fa fa-user fa-fw"></i>
              <span className="hide-menu"> Users </span>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}
const mapStateToProps = (state) => ({
  user: state.auth.user,
});
export default connect(mapStateToProps, { LogoutApi })(Sidebar);
