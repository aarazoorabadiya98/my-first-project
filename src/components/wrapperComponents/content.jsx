import React, { useEffect, Suspense, useMemo, useContext } from "react";
import { Redirect } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import Footer from "./footer";
import Loader from "../reusables/Loader";
import global from "../../global";
const Error404 = React.lazy(() => import("../reusables/Error404"));
const Dashboard = React.lazy(() => import("../contentComponents/Dashboard"));
const AdminDetails = React.lazy(() =>
  import("../contentComponents/AdminDetails")
);

let routes = [];
function Content() {
  const context = useContext(global.LoaderContext);
  const contextprops = useMemo(() => {
    return { hideLoader: context.hideLoader, isLoading: context.isLoading };
  }, [context]);
  const { hideLoader, isLoading } = contextprops;

  useEffect(() => {
    setRouteArray();
  }, []);

  useEffect(() => {
    if (isLoading) hideLoader();
  }, []);

  const setRouteArray = () => {
    routes = [
      {
        path: "/",
        exact: true,
        main: () => <Redirect to="/dashboard" />,
      },
      {
        path: "/dashboard",
        exact: true,
        main: Dashboard,
      },

      {
        path: "/users",
        main: AdminDetails,
      },
    ];
  };

  return (
    <div id="page-wrapper">
      <Suspense fallback={<Loader />}>
        <Switch>
          {routes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              strict
              render={() => <route.main />}
            />
          ))}
          <Route render={() => <Error404 />} />
        </Switch>
      </Suspense>
      <Footer />
    </div>
  );
}
export default Content;
