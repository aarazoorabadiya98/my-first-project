import React, { Component } from "react";
import { Link } from "react-router-dom";

class PageHeader extends Component {
  render() {
    // let {title, breadcrumb, activeValue, subTitle, linkValue } = this.props;
    let { mainPage, subPage, linkValue, title } = this.props;

    // let li = breadcrumb
    //   ? breadcrumb.map((tt, index) => {
    //       return (
    //         <li key={index}>
    //           <Link to="#">{tt}</Link>
    //         </li>
    //       );
    //     })
    //   : "";

    return (
      <div className="row bg-title">
        <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 className="page-title">{subPage !== "" ? subPage : title}</h4>{" "}
        </div>
        <div className="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          {/* <ol className="breadcrumb"> */}
          {/* {title!=="Dashboard"?<li><Link to="">Dashboard</Link></li>:""} */}
          {/* {li}
            <li className="active">{title}</li>
          </ol> */}
          <ol className="breadcrumb">
            {mainPage && <li>{mainPage} </li>}
            {subPage && (
              <li>
                <Link to={linkValue} style={{ color: "#686868" }}>
                  {subPage}
                </Link>
              </li>
            )}
            <li className="active">{title}</li>
          </ol>
        </div>
      </div>
    );
  }
}
export default PageHeader;
