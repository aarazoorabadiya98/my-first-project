import React, { Component } from 'react';
import './Loader.scss';
class Loader extends Component {
    
    render() {

        return (
            <div id="loader" style={{
				position:"fixed",
				top:0,
				bottom:0,
				left:0,
				right:0,
				background:"#fff",
				zIndex:9999,
			}}>
				<div className="cssload-speeding-wheel loader"></div>
			</div>
            // <div className="preloader">
            //     <div className="cssload-speeding-wheel loader"></div>
            // </div>
        );
    }
}
export default Loader;
