import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import Loader from "react-loader-spinner";
import Toolbar from "./customToolbar";
import { isEqual } from "lodash";
class Datatable extends Component {
  state = {
    limit: 100,
    skip: 0,
    data: [],
    totalRow: 0,
    currentPage: 1,
    totalPages: 0,
    rowsPerPage: 100,
    askForConfirmation: false,
    footer: true,
  };
  componentWillReceiveProps = (nextProps) => {

    if (this.props !== nextProps) {
      this.setState({
        ...(nextProps.footer === undefined && { footer: true }),
        ...(nextProps.footer !== undefined && { footer: nextProps.footer }),
      });
      if (this.props.data !== nextProps.data) {
        // this.setState({ data: [] }, () => {
        this.setState({ data: nextProps.data });
        // })
      }
      this.setState(
        {
          limit: nextProps.limit,
          skip: nextProps.skip,
          rowsPerPage: nextProps.limit,
          totalRow: nextProps.totalRow,
          ...(nextProps.skip === 0 && { currentPage: 1 }),
        },
        () => {
          this.countTotalPages();
        }
      );
    }
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (!isEqual(this.state, nextState) || !isEqual(this.props, nextProps)) {
      return true;
    } else {
      return false;
    }
  };

  countTotalPages = () => {
    let pages = Math.ceil(this.state.totalRow / this.state.limit);

    this.setState({
      totalPages: pages === 0 ? 1 : pages,
    });
  };

  goToNextPrev = (action) => {
    let newSkip, currentPage;
    if (action === "Next") {
      newSkip = Number(this.state.skip) + Number(this.state.limit);
      currentPage = this.state.currentPage + 1;
    } else {
      newSkip = Number(this.state.skip) - Number(this.state.limit);
      currentPage = this.state.currentPage - 1;
    }
    this.setState(
      {
        currentPage: currentPage,
      },
      () => {
        this.props.changeLimitSkip(this.state.limit, newSkip);
      }
    );
  };

  changeRowsPerPage = (e) => {
    this.props.changeLimitSkip(Number(e.target.value), 0);
  };

  customFooter = (count, page, rowsPerPage, changeRowsPerPage, changePage) => {
    let { currentPage, totalPages, totalRow } = this.state;
    
    return (
      <tfoot>
        <tr>
          <td colSpan="5  ">
            <div className="pull-right m-r-30 p-10">
              <div className="pull-left p-10">
                Rows per page :&nbsp;
                <select
                  name="rowsPerPage"
                  className="form-control custom-select my-select"
                  value={this.state.rowsPerPage}
                  onChange={this.changeRowsPerPage}
                >
                  <option value="10">10&nbsp;</option>
                  <option value="15">15&nbsp;</option>
                  <option value="20">20&nbsp;</option>
                  <option value="100">100&nbsp;</option>
                </select>
              </div>
              <div className="pull-left p-10 p-t-20">
                Total Records: {totalRow}
              </div>
              {this.props.isPagination === false ? (
                ""
              ) : (
                <div className="pull-right p-10 padding-top-15">
                  <span className="p-10 m-t-5 m-r-20">
                    Current Page :{" "}
                    <span className="font-bold"> {currentPage}</span> /{" "}
                    {this.state.totalPages}
                  </span>
                  <button
                    className=" btn-circle btn-default m-r-5 font-bold"
                    onClick={() => this.goToNextPrev("Prev")}
                    disabled={currentPage === 1 ? true : false}
                  >
                    <i className="fa fa-angle-left"></i>
                  </button>
                  <button
                    className=" btn-circle btn-default font-bold"
                    onClick={() => this.goToNextPrev("Next")}
                    disabled={currentPage === totalPages ? true : false}
                  >
                    <i className="fa fa-angle-right"></i>
                  </button>
                </div>
              )}
            </div>
          </td>
        </tr>
      </tfoot>
    );
  };

  customToolbar = () => {
    return (
      <Toolbar
        searchText={this.props.searchText}
        handleSearch={this.props.handleSearch}
        export={this.props.export}
        exportToExcel={this.props.exportToExcel}
        data={this.state.data}
        searchIcon={this.props.searchIcon}
      />
    );
  };

  render() {
    let { columns, serverSide, isDataLoading, selectableRows } = this.props;
    let { rowsPerPage } = this.state;
    let options = {
      responsive: "scroll",
      // selectableRows: false,
      filter: false,
      sort: false,
      print: false,
      download: false,
      viewColumns: false,
      rowsPerPage: rowsPerPage,
      ...(selectableRows && {
        customToolbarSelect: this.props.customSelectToolBar,
      }),
      selectableRows: selectableRows ? "multiple" : false,
      ...(selectableRows && { rowsSelected: this.props.rowsSelectedIndexes }),
      ...(selectableRows && {
        onRowsSelect: (currentRowsSelected, allRowsSelected) =>
          this.props.setSelectedRows(currentRowsSelected, allRowsSelected),
      }),
      textLabels: {
        body: {
          noMatch: isDataLoading ? (
            <Loader type="ThreeDots" color="gray" width="35" height="35" />
          ) : (
            // <div style={{ height: "100px", verticalAlign: "middle" }}>
            //     <Loader
            //         type="ThreeDots"
            //         color="gray"
            //         width="35"
            //         height="35"
            //     />
            // </div>
            "Sorry, there is no matching data to display"
          ),
        },
      },
    };
    options = serverSide
      ? {
          ...options,
          search: false,
          // onTableChange: this.onTableChange,
          customToolbar: this.customToolbar,
          customFooter: () => this.customFooter(),
        }
      : { ...options,search: false,};
    return (
      <>
        <MUIDataTable
          data={this.state.data}
          columns={columns}
          options={options}
        />
      </>
    );
  }
}
export default Datatable;
