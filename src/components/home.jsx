// eslint-disable-next-line
import React, { useEffect, useState, Component, Suspense } from "react";
import Header from "./wrapperComponents/Header";
import Sidebar from "./wrapperComponents/sidebar";
import global from "../global";

import { connect } from "react-redux";
import * as $ from "jquery";
import Content from "./wrapperComponents/content";
import Loader from "./reusables/Loader";
import { split } from "lodash";

function Home(props) {
  const [isLoading, setIsLoading] = useState(false);
  const [showDotBell] = useState(false);

  const setMenuItemActive = (pathName) => {
    $("#side-menu li a").removeClass("active");
    $("#side-menu li ").removeClass("active");
    $("#side-menu")
      .find("ul.nav.nav-second-level")
      .removeClass("in")
      .attr("aria-expanded", false);
    $("#side-menu")
      .find('a[href^="/' + pathName[1] + '"]')
      .addClass("active");
    $("#side-menu")
      .find('a[href^="/' + pathName[1] + '"]')
      .parent("li")
      .parent("ul")
      .siblings("a")
      .addClass("active");
    $("#side-menu")
      .find('a[href^="/' + pathName[1] + '"]')
      .parent("li")
      .parent("ul.nav.nav-second-level")
      .addClass("in")
      .attr("aria-expanded", true);
  };
  const showLoader = () => {
    setIsLoading(true);
  };
  const hideLoader = () => {
    if (isLoading === true) {
      setIsLoading(false);
    }
  };
  useEffect(() => {
    let pathName = split(props.location.pathname, "/");
    setMenuItemActive(pathName);
  }, []);

  let LoaderContext = global.LoaderContext;
  return (
    <div id="Homepage">
      {isLoading && <Loader />}
      <div id="wrapper">
        <LoaderContext.Provider
          value={{
            hideLoader: () => hideLoader(),
            showLoader: () => showLoader(),
            isLoading: isLoading,
          }}
        >
          <Header history={props.history} showDotBell={showDotBell} />
          <Sidebar history={props.history} />
          <Content />
        </LoaderContext.Provider>
      </div>
    </div>
  );
}
function mapStateToProps(state) {
  return {
    store: state.auth,
  };
}
let home = connect(mapStateToProps, {})(Home);
export default home;
