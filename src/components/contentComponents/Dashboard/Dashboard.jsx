import React from "react";
import "./Dashboard.scss";
import PageHeader from "../../reusables/PageHeader";

export default function Dashboard() {
  return (
    <div className="container-fluid">
      <PageHeader mainPage="" subPage="" title="Dashboard" />
    </div>
  );
}
