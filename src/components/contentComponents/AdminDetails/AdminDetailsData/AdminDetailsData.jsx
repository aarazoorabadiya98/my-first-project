import React from "react";
import PageHeader from "../../../reusables/PageHeader";
import { NavLink } from "react-router-dom";
import "./adminForm.scss";
import { connect } from "react-redux";
function AdminDetailsData() {
  return (
    <>
      <PageHeader mainPage="" subPage="" title="Users" />
      <div className="row">
        <div className="col-sm-12">
          <div className="white-box">
            <div className="d-flex m-b-20">
              <div className="mr-auto">
                <h3 className="box-title m-b-0">Users</h3>
              </div>

              <span>
                <NavLink
                  to="/users/form"
                  className="btn btn-danger btn-rounded btn-outline"
                >
                  Add new user
                </NavLink>
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default connect(null, {})(AdminDetailsData);
