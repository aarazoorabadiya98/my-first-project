import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PageHeader from "../../../reusables/PageHeader";

function AdminDetailsForm() {
  return (
    <div>
      <PageHeader
        mainPage=""
        subPage="Users"
        linkValue="/users"
        title="Add users"
      />
    </div>
  );
}
export default withRouter(connect(null, {})(AdminDetailsForm));
