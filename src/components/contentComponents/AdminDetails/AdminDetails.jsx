import React, { useState } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
const AdminDetailsData = React.lazy(() => import("./AdminDetailsData"));
const AdminDetailsForm = React.lazy(() => import("./AdminDetailsForm"));

function AdminDetails() {
  const [searchText, setSearchText] = useState("");

  const handleSearch = (searchText) => {
    setSearchText(searchText);
  };

  return (
    <div className="container-fluid">
      <Switch>
        <Route
          exact
          strict
          path="/users"
          render={() => (
            <AdminDetailsData
              handleSearch={handleSearch}
              searchText={searchText}
            />
          )}
        />

        <Route
          exact
          strict
          path="/users/form/:id?"
          render={() => <AdminDetailsForm />}
        />
      </Switch>
    </div>
  );
}
export default withRouter(AdminDetails);
