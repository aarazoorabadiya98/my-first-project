import React, { Component } from "react";

class RecoverPassword extends Component {
  state = {
    emailOrPhone: "",
    invalidEmail: false,
    invalidPhone: false,
    isPhone: false,
    isEmail: false,
    isDisabled: true,
  };

  validateEmail = () => {
    if (
      !this.state.emailOrPhone.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    ) {
      this.setState({
        invalidEmail: true,
      });
    } else {
      this.setState({
        invalidEmail: false,
        isDisabled: false,
      });
    }
  };

  validatePhone = () => {
    let input = this.state.emailOrPhone;
    if (!input.match(/^\+\d{7,12}$/i)) {
      this.setState({
        invalidPhone: true,
      });
    } else {
      this.setState({
        invalidPhone: false,
        isDisabled: false,
      });
    }
  };

  resetState = () => {
    this.setState({
      isPhone: false,
      isEmail: false,
      invalidEmail: false,
      invalidPhone: false,
    });
  };

  checkForEmailOrPhone = () => {
    let input = this.state.emailOrPhone;
    if (input !== "") {
      if (input.match(/[0-9 -()+]+$/i)) {
        this.validatePhone();
        this.setState({
          isPhone: true,
          isEmail: false,
        });
      } else {
        this.validateEmail();
        this.setState({
          isEmail: true,
          isPhone: false,
        });
      }
    } else {
      this.resetState();
    }
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let payload = {
      value: this.state.emailOrPhone,
    };
    if (this.state.isEmail) {
      payload.type = "email";
    } else if (this.state.isPhone) {
      payload.type = "phone";
    }
  };
  render() {
    let {
      emailOrPhone,
      invalidEmail,
      isEmail,
      isPhone,
      invalidPhone,
      isDisabled,
    } = this.state;
    let disabled =
      emailOrPhone && !invalidEmail && !invalidPhone && !isDisabled
        ? ""
        : "disabled";
    return (
      <div>
        {/* <Loader /> */}
        <section id="wrapper" className="login-register">
          <div className="login-box">
            <div className="white-box">
              {/* {error} */}
              <form className="form-horizontal">
                <div className="form-group ">
                  <div className="col-xs-12">
                    <h3>Recover Password</h3>
                    {/* <p className="text-muted">Enter your Email or phone number and instructions will be sent to you! </p> */}
                  </div>
                </div>
                <div className="form-group ">
                  <div className="col-xs-12">
                    <input
                      className="form-control"
                      required
                      type="text"
                      value={emailOrPhone}
                      placeholder="Enter email or phone with country code "
                      onBlur={() => this.checkForEmailOrPhone()}
                      onChange={(e) =>
                        this.setState({ emailOrPhone: e.target.value })
                      }
                    />
                    {isEmail && invalidEmail && (
                      <span className="text-danger">
                        Please enter valid email.
                      </span>
                    )}
                    {isPhone && invalidPhone && (
                      <span className="text-danger">
                        Please enter valid phone number.
                      </span>
                    )}
                  </div>
                </div>
                <div className="form-group text-center m-t-20">
                  <div className="col-xs-12">
                    <button
                      className="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                      type="submit"
                      onClick={(e) => this.handleSubmit(e)}
                      disabled={disabled}
                    >
                      Send OTP
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default RecoverPassword;
