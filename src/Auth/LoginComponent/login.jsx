import React, { Component } from "react";
import global from "../../global";
import {
  loginApi,
  sendRecoverPasswordEmail,
  CheckTwoStepAuth,
} from "../../actions";
import { connect } from "react-redux";
import Loader from "../../components/reusables/Loader";
import postscribe from "postscribe";
let script =
  '<script src="/styles/js/custom.min.js"></script>' +
  '<script src="/styles/js/jquery.slimscroll.js"></script>' +
  '<script src="/styles/js/validator.js"></script>' +
  '<script src="/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>';
class Login extends Component {
  state = {
    data: null,
    loginEmail: "",
    password: "",
    alertStyle: "",
    alert: "",
    token: "",
    isLoading: true,
    showLoginForm: true,
    showRecoverPasswordForm: false,
    showPassword: false,
    showToken: false,
    formErrors: {
      loginEmail: false,
      email: false,
      password: false,
      token: false,
    },
    Authval: false,
    invalidPassword: "Password should not contain white space.",
    invalidEmail: "Email format should be 'example@example.com'.",
  };
  componentWillMount = () => {
    if (localStorage.getItem("auth_Token") !== null) {
      this.props.history.length > 2
        ? this.props.history.goBack()
        : this.props.history.push("/");
      global.check_Auth(this.props.history);
    }
  };

  componentDidMount = () => {
    this.setState({
      isLoading: false,
    });
  };

  handleChange = (e) => {
    var element = e.target.name;
    let inputVal = e.target.value;
    let val = true;
    if (element === "email" || element === "loginEmail") {
      // val = inputVal.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      val = global.validationForEmail(inputVal);
      this.setState({
        Authval: false,
      });
    } else if (element === "password") {
      val = !inputVal.match(/\s/g);
    }
    if (element === "token") {
      val = inputVal.match(/\d/g);
    }
    if (val) {
      this.setState({
        [element]: inputVal,
        formErrors: { ...this.state.formErrors, [element]: false },
      });
    } else {
      this.setState({
        [element]: inputVal,
        formErrors: { ...this.state.formErrors, [element]: true },
      });
    }
  };
  Login = () => {
    let payload = {
      email: this.state.loginEmail,
      password: this.state.password,
      // ...(this.state.token && { token: this.state.token }),
    };
    if (
      payload.email === "admin@gmail.com" &&
      payload.password === "admin123"
    ) {
      localStorage.setItem("auth_Token", "xyzapqrrsatydapbmcodielfn");
      this.props.history.replace("/dashboard");
      postscribe("#root", script);
    }
    // this.props.loginApi(payload).then((res) => {
    //   if (res.response.status === 200) {
    //     let dashboardRole = res.responseData.data.role[0].access.dashboard;
    //     let prevPath = this.props.location.state
    //       ? this.props.location.state.from.pathname
    //       : null;
    //     let redirectPath = "";
    //     if (dashboardRole !== "none") {
    //       redirectPath = "/dashboard";
    //     } else {
    //       redirectPath = "/edit-profile";
    //     }
    //     let path =
    //       prevPath === null || prevPath === "/" ? redirectPath : prevPath;

    //     this.props.history.replace(path);
    //     postscribe("#root", script);
    //   } else {
    //     this.setState({
    //       alert:
    //         res.response.status === 400
    //           ? global.errors.error400
    //           : res.responseData.message,
    //       alertStyle: "danger",
    //     });
    //   }
    // });
  };
  handleLogin = async (e) => {
    e.preventDefault();
    this.Login();
  };

  sendRecoverPasswordEmail = (e) => {
    e.preventDefault();
    let payload = {
      email: this.state.email,
    };
    this.props.sendRecoverPasswordEmail(payload).then((res) => {
      if (res.response.status === 200) {
        this.setState({
          alert: "Please check your mail",
          alertStyle: "success",
        });
      } else {
        this.setState({
          alert:
            res.response.status === 400
              ? global.errors.error400
              : res.responseData.message,
          alertStyle: "danger",
        });
      }
    });
  };

  toggleForms = () => {
    this.setState({
      showLoginForm: !this.state.showLoginForm,
      showRecoverPasswordForm: !this.state.showRecoverPasswordForm,
      alert: "",
    });
  };

  showPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };
  showToken = () => {
    this.setState({
      showToken: !this.state.showToken,
    });
  };
  render() {
    let {
      loginEmail,
      password,
      email,
      isLoading,
      showLoginForm,
      showRecoverPasswordForm,
      formErrors,
      invalidEmail,
      invalidPassword,
      showPassword,
      showToken,
      Authval,
      token,
    } = this.state;

    const isIExplorer = false || !!document.documentMode;
    const isEdge = !isIExplorer && !!window.StyleMedia;

    const error =
      this.state.alert === "" ? (
        ""
      ) : (
        <div className={"alert alert-" + this.state.alertStyle}>
          <b>{this.state.alert}</b>
        </div>
      );
    let tokenError = true;
    if (Authval) {
      tokenError = false;
      if (token !== "") {
        tokenError = true;
      }
    }

    // }
    let disabled =
      loginEmail &&
      password &&
      !formErrors.loginEmail &&
      !formErrors.password &&
      tokenError
        ? ""
        : "disabled";

    return (
      <>
        {isLoading && <Loader />}
        <section id="wrapper" className="background-wrapper">
          <div className="white-box">
            {error}
            {showLoginForm && (
              <form
                className="form-horizontal form-material"
                // id="loginform"
              >
                <h3 className="box-title m-b-20">Sign In</h3>
                <div className="form-group ">
                  <div className="col-xs-12">
                    <input
                      className="form-control"
                      type="email"
                      name="loginEmail"
                      value={loginEmail}
                      onFocus={() => this.setState({ alert: "" })}
                      onChange={(e) => this.handleChange(e)}
                      placeholder="Email"
                      maxLength="255"
                    />
                    {formErrors.loginEmail && (
                      <span className="text-danger">{invalidEmail}</span>
                    )}
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-xs-12">
                    <div className="position-relative">
                      {/* <div className="col-xs-12 d-flex align-items-center justify-content-center"> */}
                      <input
                        className="form-control px-1"
                        type={showPassword ? "text" : "password"}
                        name="password"
                        value={password}
                        onFocus={() => this.setState({ alert: "" })}
                        onChange={(e) => this.handleChange(e)}
                        required
                        placeholder="Password"
                        maxLength="255"
                      />
                      {isIExplorer || isEdge ? (
                        <></>
                      ) : (
                        <div
                          className="iconInInput"
                          onClick={() => this.showPassword()}
                        >
                          <i
                            className={
                              showPassword ? "fa fa-eye" : "fa fa-eye-slash"
                            }
                          ></i>
                        </div>
                      )}
                    </div>
                    {formErrors.password && (
                      <span className="text-danger">{invalidPassword}</span>
                    )}
                  </div>
                </div>
                {Authval && (
                  <div className="form-group ">
                    <div className="col-xs-12">
                      <input
                        className="form-control px-1"
                        type={showToken ? "text" : "password"}
                        name="token"
                        value={token}
                        onChange={(e) => this.handleChange(e)}
                        placeholder="Google Token"
                        maxLength="6"
                      />
                      <div
                        className="tokenInput"
                        onClick={() => this.showToken()}
                      >
                        <i
                          className={
                            showToken ? "fa fa-eye" : "fa fa-eye-slash"
                          }
                        ></i>
                      </div>
                      {formErrors.token && (
                        <span className="text-danger">
                          Token should be degits.
                        </span>
                      )}
                    </div>
                  </div>
                )}
                <div className="form-group">
                  <div className="col-md-12">
                    <div
                      className="text-dark pull-right c-pointer"
                      onClick={() => this.toggleForms()}
                    >
                      <i className="fa fa-lock m-r-5"></i>
                      Forgot password?
                    </div>
                  </div>
                </div>
                <div className="form-group text-center m-t-20">
                  <div className="col-xs-12">
                    <button
                      // disabled={this.disableButton()}
                      disabled={disabled}
                      onClick={(e) => this.handleLogin(e)}
                      className="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                      type="submit"
                    >
                      Log In
                    </button>
                  </div>
                </div>
              </form>
            )}
            {showRecoverPasswordForm && (
              <>
                <div
                  className="c-pointer"
                  onClick={() => {
                    this.toggleForms();
                  }}
                >
                  <i className="fa fa-arrow-left"></i> Back
                </div>
                <form
                  className="form-horizontal"
                  // id="recoverform"
                >
                  <div className="form-group m-b-0">
                    <div className="col-xs-12">
                      <h3>Recover Password</h3>
                      <p className="text-muted">
                        Enter your Email and instructions will be sent to you!{" "}
                      </p>
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="col-xs-12">
                      <input
                        className="form-control"
                        type="email"
                        name="email"
                        value={email ? email : ""}
                        onFocus={() => this.setState({ alert: "" })}
                        onChange={(e) => this.handleChange(e)}
                        placeholder="Email"
                        maxLength="255"
                      />
                      {formErrors.email && (
                        <span className="text-danger">{invalidEmail}</span>
                      )}
                    </div>
                  </div>
                  <div className="form-group text-center m-t-20">
                    <div className="col-xs-12">
                      <button
                        className="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                        type="submit"
                        disabled={email && !formErrors.email ? "" : "disabled"}
                        onClick={(e) => this.sendRecoverPasswordEmail(e)}
                      >
                        Reset
                      </button>
                    </div>
                  </div>
                </form>
              </>
            )}
            {/* </div> */}
          </div>
        </section>
      </>
    );
  }
}
function mapStateToProp(state) {
  return {
    store: state.auth,
  };
}
export default connect(mapStateToProp, {
  loginApi,
  sendRecoverPasswordEmail,
  CheckTwoStepAuth,
})(Login);
