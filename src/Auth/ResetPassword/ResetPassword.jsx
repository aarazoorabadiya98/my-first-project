import React, { Component } from "react";
import { connect } from "react-redux";
import { resetPassword, checkForToken } from "../../actions";
import Loader from "react-loader-spinner";
import warning from "../../assets/images/error.png";
import CustomLoader from "../../components/reusables/Loader";
class ResetPassword extends Component {
  state = {
    password: "",
    confirmPassword: "",
    formErrors: {
      password: false,
      confirmPassword: false,
    },
    token: "",
    alertStyle: "",
    alert: "",
    isLoading: false,
    showPassword: false,
    showConfirmPassword: false,
    tokenAlreadyUsed: false,
    invalidTokenMessage: "",
    showLoader: true,
  };
  componentWillMount = () => {
    this.setState(
      {
        showLoader: false,
        token: window.location.pathname.split("/")[2],
      },
      () => {
        this.props.checkForToken(this.state.token).then((res) => {
          this.setState({
            showLoader: false,
          });
          if (res.response.status === 200) {
            this.setState({
              tokenAlreadyUsed: false,
              invalidTokenMessage: "",
            });
          } else if (
            res.response.status === 404 ||
            res.response.status === 202
          ) {
            this.setState({
              tokenAlreadyUsed: true,
              invalidTokenMessage: res.responseData.message,
            });
          }
        });
      }
    );
  };
 

  handleChange = (e) => {
    var element = e.target.name;
    let inputVal = e.target.value;
    let val = true;
    if (element === "password") {
     
      val = global.validationForPassword(inputVal);
      if (val) {
        let { confirmPassword, formErrors } = this.state;
        if (confirmPassword) {
          if (confirmPassword !== inputVal) {
            formErrors.confirmPassword = true;
          } else {
            formErrors.confirmPassword = false;
          }
          this.setState({
            formErrors,
          });
        }
      }
    } else if (element === "confirmPassword") {
      val = this.state.password === inputVal ? true : false;
    }
    if (val) {
      this.setState({
        [element]: inputVal,
        isDisabled: false,
        formErrors: { ...this.state.formErrors, [element]: false },
      });
    } else {
      this.setState({
        [element]: inputVal,
        formErrors: { ...this.state.formErrors, [element]: true },
      });
    }
  };

  clearState = () => {
    this.setState({
      password: "",
      confirmPassword: "",
      formErrors: {
        password: false,
        confirmPassword: false,
      },
    });
  };

  resetAlert = () => {
    this.setState({
      alertStyle: "",
      alert: "",
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      isLoading: true,
    });
    let payload = {
      token: this.state.token,

      newPassword: this.state.password,
    };
    this.props.resetPassword(payload).then((res) => {
      this.setState({
        isLoading: false,
      
      });
      this.props.history.replace("/login");

      this.clearState();
    });
  };
  showPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };
  showConfirmPassword = () => {
    this.setState({
      showConfirmPassword: !this.state.showConfirmPassword,
    });
  };
  render() {
    let {
      showLoader,
      password,
      confirmPassword,
      formErrors,
      isLoading,
      showPassword,
      showConfirmPassword,
      tokenAlreadyUsed,
      invalidTokenMessage,
    } = this.state;
    let noError = null;
    let formElementAry = Object.keys(formErrors);
    for (let i = 0; i < formElementAry.length; i++) {
      if (formErrors[formElementAry[i]] === false) {
        noError = true;
      } else {
        noError = false;
        break;
      }
    }
    let passwordMatch = false;
    if (password === confirmPassword) {
      passwordMatch = true;
    }
    let disabled = noError && passwordMatch && password ? "" : "disabled";
    const error =
      this.state.alert === "" ? (
        ""
      ) : (
        <div className={"alert alert-" + this.state.alertStyle}>
          <b>{this.state.alert}</b>
        </div>
      );

    return (
      <>
        <section id="wrapper" className="background-wrapper">
          {showLoader && <CustomLoader />}
          <div className="white-box">
            {tokenAlreadyUsed ? (
              <div className="text-center">
                <div>
                  <img src={warning} alt="warning" />
                </div>
                {/* <h4 className="m-b-0 m-t-20">Seems You have already reset your password.</h4> */}
                <h4 className="m-b-0 m-t-20">{invalidTokenMessage}</h4>
              </div>
            ) : (
              <>
                {error}
                <form className="form-horizontal">
                  <div className="form-group">
                    <div className="col-xs-12">
                      <h3>Reset Password</h3>
                      {/* <p className="text-muted">Enter your Email or phone number and instructions will be sent to you! </p> */}
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="password" className="control-label">
                      Password
                    </label>
                    <br />
                    <div className="position-relative">
                      <input
                        type={showPassword ? "text" : "password"}
                        // type="password"
                        className="form-control"
                        name="password"
                        placeholder="Enter password"
                        value={password}
                        onChange={(e) => this.handleChange(e)}
                        onFocus={() => this.resetAlert()}
                      />
                      <div
                        className="iconInInput"
                        onClick={() => this.showPassword()}
                      >
                        <i
                          className={
                            showPassword ? "fa fa-eye-slash" : "fa fa-eye"
                          }
                        ></i>
                      </div>
                    </div>
                    {formErrors.password ? (
                      <span className="text-danger">
                        {/* Password length should 6 to 15. */}
                        Password length should be least 6 digit long and
                        contains both letters and numbers.
                      </span>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="form-group">
                    <label htmlFor="password" className="control-label">
                      Confirm password
                    </label>
                    <br />
                    <div className="position-relative">
                      <input
                        // type="password"
                        type={showConfirmPassword ? "text" : "password"}
                        className="form-control"
                        name="confirmPassword"
                        placeholder="Re-type password"
                        value={confirmPassword}
                        onChange={(e) => this.handleChange(e)}
                      />
                      <div
                        className="iconInInput"
                        onClick={() => this.showConfirmPassword()}
                      >
                        <i
                          className={
                            showConfirmPassword
                              ? "fa fa-eye-slash"
                              : "fa fa-eye"
                          }
                        ></i>
                      </div>
                    </div>
                    {formErrors.confirmPassword ? (
                      <span className="text-danger">
                        Password does not match.
                      </span>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="form-group text-center m-t-20">
                    <div className="col-xs-12">
                      <button
                        className="btn btn-info btn-lg btn-block font-light waves-effect waves-light"
                        type="submit"
                        onClick={(e) => this.handleSubmit(e)}
                        disabled={disabled}
                      >
                        {isLoading ? (
                          <span className="d-flex align-items-center justify-content-center">
                            <Loader
                              type="Oval"
                              color="#fff"
                              width="18"
                              height="18"
                            />
                            &nbsp;Reset Password
                          </span>
                        ) : (
                          "Reset Password"
                        )}
                      </button>
                    </div>
                  </div>
                </form>
              </>
            )}
          </div>
        </section>
      </>
    );
  }
}
export default connect(null, { resetPassword, checkForToken })(ResetPassword);
