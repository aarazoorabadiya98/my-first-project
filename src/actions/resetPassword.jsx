export const checkForToken = (token) => (dispatch, getState, Api) => {
  return Api.getWithBasicHeader("resetPassword/" + token).then((response) => {
    return response;
  });
};
export const resetPassword = (payload) => (dispatch, getState, Api) => {
  return Api.put("resetPassword", payload, { basicAuth: true }).then(
    (response) => {
      return response;
    }
  );
};

export const sendRecoverPasswordEmail = (payload) => (
  dispatch,
  getState,
  Api
) => {
  return Api.post("resetPassword/create", payload, "header").then((res) => {
    return res;
  });
};
