import { LOGIN_SUCCESS, PREVPATH, LOGOUT } from "./types";
import history from "../history";
import { has } from "lodash";
export const loginSuccess = (user) => {
  let user_data = {};
  if (user.authToken) {
    user_data = {
      auth_Token: user.authToken,
    };
    localStorage.setItem("auth_Token", user.authToken);
  } else {
    user_data = {
      auth_Token: localStorage.getItem("auth_Token"),
    };
  }
  if (has(user, "name")) {
    delete user.authToken;
    user_data.data = user;
    localStorage.setItem("user", JSON.stringify(user));
  } else {
    let userData = JSON.parse(localStorage.getItem("user"));
    user_data.data = userData;
  }
  return {
    type: LOGIN_SUCCESS,
    user: user_data,
  };
};

export const loginApi = (payload) => (dispatch, getState, Api) => {
  return Api.post("admin/login", payload, "header").then((res) => {
    if (res.response.status === 200) {
      dispatch(loginSuccess(res.responseData.data));
    }
    return res;
  });
};
export const CheckTwoStepAuth = (email) => (dispatch, getState, Api) => {
  return Api.get("admin/CheckTwoStepAuth/" + email).then((response) => {
    return response;
  });
};
export const LogoutApi = () => (dispatch, getState, Api) => {
  // Api.delete("admin/logout").then((res) => {
  //   if (res.response.status === 200) {
  dispatch(logoutSuccess());
  //   }
  // });
  return {
    type: LOGOUT,
  };
};

export const logoutSuccess = () => {
  localStorage.clear();
  history.push("/login");

  return {
    type: LOGOUT,
  };
};

export const storePrevLocation = () => {
  let path = window.location.pathname;
  return {
    type: PREVPATH,
    path: path,
  };
};
