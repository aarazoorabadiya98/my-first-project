export const getAdminDetail = (payload) => (dispatch, getState, Api) => {
  return Api.post(
    "admin/" + payload.limit + "/" + payload.skip,
    payload.data
  ).then((response) => {
    return response;
  });
};

export const getAdminTwostepAuth = () => (dispatch, getState, Api) => {
  return Api.get("admin/twoStepAuth").then((response) => {
    return response;
  });
};

export const editAdmin2FAStatus = (id, payload) => (
  dispatch,
  getState,
  Api
) => {
  return Api.put("admin/twoStepPermission/" + id, payload).then((response) => {
    return response;
  });
};

export const getAdminInfo = (id) => (dispatch, getState, Api) => {
  return Api.get("admin/" + id).then((response) => {
    return response;
  });
};

export const addAdminDetails = (payload) => (dispatch, getState, Api) => {
  return Api.post("admin", payload).then((response) => {
    return response;
  });
};

export const editAdminDetails = (payload, id) => (dispatch, getState, Api) => {
  return Api.put("admin/" + id, payload).then((response) => {
    return response;
  });
};

export const deleteAdminDetail = (id) => (dispatch, getState, Api) => {
  return Api.delete("admin/" + id).then((response) => {
    return response;
  });
};

export const editAdminStatus = (payload, id) => (dispatch, getState, Api) => {
  return Api.put("admin/status/" + id, payload).then((response) => {
    return response;
  });
};

//roles
export const getAllRolesDetails = () => (dispatch, getState, Api) => {
  return Api.get(
    "admin/roles"
   
  ).then((response) => {
    return response;
  });
};

export const getRolesDetails = (id) => (dispatch, getState, Api) => {
  return Api.get("admin/roles/" + id).then((response) => {
    return response;
  });
};

export const addRolesDetails = (payload) => (dispatch, getState, Api) => {
  return Api.post("admin/roles", payload).then((response) => {
    return response;
  });
};

export const editRolesDetails = (id, payload) => (dispatch, getState, Api) => {
  return Api.put("admin/roles/" + id, payload).then((response) => {
    return response;
  });
};

export const deleteRolesDetail = (id) => (dispatch, getState, Api) => {
  return Api.delete("admin/roles/" + id).then((response) => {
    return response;
  });
};
