import { combineReducers } from "redux";
import auth from "./auth";
import dashboard from "./dashboard";
import { LOGOUT } from "../actions/types";

const appReducer = combineReducers({
  auth,
  dashboard,
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
