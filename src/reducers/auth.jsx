import { LOGIN_SUCCESS, PREVPATH } from '../actions/types';

const initialState = {
  user: null,
  prevPath: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,user:action.user
      };
    case PREVPATH:
      return {
        ...state,
        prevPath: action.path
      };
    default:
      return state;
  }
}