import React from "react";
import moment from "moment";
import CountryCode from "./CountryCode";

const LoaderContext = React.createContext();
const DriverFilterContext = React.createContext();
const CustomerFilterContext = React.createContext();
const BookingFilterContext = React.createContext();
export default {
  config: {
    apiUrl: "",
    mapboxAccessToken: "",
    googleApiKey: "",
    getMatiClientId: "",

    projectId: "",
    appId: "",

    //https
    // mqttProps: "wss://...@mqtt....:..../mqtt",
  },

  errors: {
    error400: "Technical error!",
  },

  getToken: () => {
    return localStorage.getItem("auth_Token");
  },

  check_Auth: (props) => {
    let auth_Token = localStorage.getItem("auth_Token");
    if (auth_Token === null && props.location.pathname === "/login") {
      props.push("/login");
    } else {
      if (props.location.pathname === "/login") {
        props.goBack();
      }
    }
  },

  toCapitalize: (string) => {
    if (!string) {
      return string;
    }
    let strArray = string.split(" ");
    let newString = "";
    strArray.map((s) => {
      var word =
        s && s && s[0].toUpperCase() + s.substring(1, s.length).toLowerCase();
      if (strArray.length > 1) {
        newString += word + " ";
      } else {
        newString += word;
      }
      return null;
    });

    return newString;
  },
  LoaderContext: LoaderContext,
  DriverFilterContext: DriverFilterContext,
  CustomerFilterContext: CustomerFilterContext,
  BookingFilterContext: BookingFilterContext,
  timeZoneList: () => {
    let timezoneList = [];
    let timezones = moment.tz.names();
    timezones.length > 0 &&
      timezones.map((timezone) => {
        let offSet = moment()
          .tz(timezone)
          .format("Z");
        timezoneList = [
          ...timezoneList,
          {
            name: "(GMT " + offSet + ") " + timezone,
            value: timezone,
          },
        ];
        return timezoneList;
      });
    return timezoneList;
  },

  // Set selected timezone as default and then format date and time so that time does not change
  formatDateAndTime: (dateTime, timeZone) => {
    moment.tz.setDefault(timeZone);

    let time = dateTime.time.format("HH:mm");

    let formattedDateTime = moment(
      dateTime.date + " " + time,
      "MM/DD/YYYY HH:mm"
    ).format("YYYY-MM-DDTHH:mm:ss[Z]");

    // let utc = moment(formattedDateTime)
    // .utc()
    // .utcOffset(+3)
    // .format();
    // return moment(utc).add(3, "hours");
    return formattedDateTime;
  },
  validationForName: (name) => {
    if (name.match(/^[a-zA-Z][a-zA-Z\s]*$/)) {
      return true;
    } else {
      return false;
    }
  },
  validationForFirstAndLastName: (name) => {
    // eslint-disable-next-line no-useless-escape
    if (name.match(/^[a-zA-Z\/\.][a-zA-Z\/\.\s]*$/)) {
      return true;
    } else {
      return false;
    }
  },
  validationForEmail: (email) => {
    if (email.startsWith(" ")) {
      return false;
    } else {
      if (
        email.match(
          /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        )
      ) {
        return true;
      } else return false;
    }
    // if (email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
    // if (email.match(/^(?![\s-])([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
    //   return true;
    // } else {
    //   return false;
    // }
  },
  validationForPassword: (password) => {
    if (password.match(/^(?=.*\d)(?=.*[a-zA-Z]).{6,}$/)) {
      return true;
    } else {
      return false;
    }
  },
  validationForPhoneNumber: (phoneNumber) => {
    if (phoneNumber.startsWith("09")) {
      if (phoneNumber.match(/^\d{10}$/)) {
        if (phoneNumber.startsWith("0900")) {
          return false;
        } else {
          return true;
        }
      }
    } else if (phoneNumber.startsWith(9)) {
      if (phoneNumber.match(/^\d{9}$/)) {
        if (phoneNumber.startsWith("900")) {
          return false;
        } else {
          return true;
        }
      }
    } else {
      return false;
    }
  },
  // Compare times
  checkForTime: (fromTime, toTime) => {
    if (toTime.hour() >= fromTime.hour()) {
      if (
        toTime.hour() === fromTime.hour() &&
        toTime.minute() <= fromTime.minute()
      ) {
        // Incorrect time
        return true;
      } else {
        return false;
      }
    } else {
      // Incorrect time
      return true;
    }
  },

  getCountryWithCode: () => {
    let data = CountryCode.countryDialCode;
    let List = [];
    data.map((val, index) => {
      let Obj = {
        name: `${val.name} (${val.dial_code})`,
        value: `${val.dial_code}*${index}`,
      };
      List.push(Obj);
      return List;
    });
    return List;
  },
};
