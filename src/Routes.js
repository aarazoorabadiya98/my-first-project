import React, { Suspense, useEffect } from "react";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import Login from "./Auth/LoginComponent";
import Home from "./components/home";
import { Provider } from "react-redux";
import ResetPassword from "./Auth/ResetPassword";
// import global from "./global";
import history from "./history";
import Loader from "./components/reusables/Loader";
import { Connector } from "mqtt-react";
const Error404 = React.lazy(() => import("./components/reusables/Error404"));

const SecretRoute = ({ component: Component, ...rest }) => {
  const isAuthenticated =
    localStorage.getItem("auth_Token") === null ? false : true;
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated === true ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
function Routes(props) {
  useEffect(() => {
    if (process.env.NODE_ENV === "production") {
      if (window) {
        window.console.log = function() {};
      }
    }
    if (
      typeof window.orientation !== "undefined" ||
      navigator.userAgent.indexOf("IEMobile") !== -1
    ) {
      document
        .querySelector("body")
        .classList.remove("fix-header", "fix-sidebar");
    }
  }, []);
  return (
    <Connector
    //mqttProps={global.config.mqttProps}
    >
      <Provider store={props.store}>
        <Router history={history}>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route path="/reset-password" component={ResetPassword} />

            <SecretRoute path="/" component={Home} />
            <Route
              render={() => (
                <Suspense fallback={<Loader />}>
                  <Error404 />
                </Suspense>
              )}
            />
          </Switch>
        </Router>
      </Provider>
    </Connector>
  );
}

// class Routes extends Component {
//   componentDidMount() {
//     if (process.env.NODE_ENV === "production") {
//       if (window) {
//         window.console.log = function() {};
//       }
//     }
//     if (
//       typeof window.orientation !== "undefined" ||
//       navigator.userAgent.indexOf("IEMobile") !== -1
//     ) {
//       document
//         .querySelector("body")
//         .classList.remove("fix-header", "fix-sidebar");
//     }
//   }

//   render() {
//     return (
//       <Connector
//       //mqttProps={global.config.mqttProps}
//       >
//         <Provider store={this.props.store}>
//           <Router history={history}>
//             <Switch>
//               <Route exact path="/login" component={Login} />
//               <Route path="/reset-password" component={ResetPassword} />

//               <SecretRoute path="/" component={Home} />
//               <Route
//                 render={() => (
//                   <Suspense fallback={<Loader />}>
//                     <Error404 />
//                   </Suspense>
//                 )}
//               />
//             </Switch>
//           </Router>
//         </Provider>
//       </Connector>
//     );
//   }
// }
export default Routes;
