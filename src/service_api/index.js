import global from "../global";
import store from "../configStore";
import { logoutSuccess } from "../actions";

const globalUrl = global.config.apiUrl;
const apiUrlV2 = global.config.apiUrlV2;

export default {
  post: (url, data, headerData, version) => {
    let header =
      headerData === "header"
        ? {
            method: "POST",
            body: JSON.stringify(data),
          }
        : {
            method: "POST",
            headers: {
              authorization: global.getToken(),
            },
            body: JSON.stringify(data),
          };

    let apiUrl = version === "v2" ? apiUrlV2 + url : globalUrl + url;
    return fetch(apiUrl, header)
      .then((response) => {
        return response
          .json()
          .then((json) => {
            if (response.status === 401) {
              store.dispatch(logoutSuccess());
            }
            return Promise.resolve({
              responseData: json,
              response: response,
            });
          })
          .catch((err) => {
            return Promise.resolve({
              response: response,
            });
          });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },
  postForm: (url, data, headerData, version) => {
    let header =
      headerData === "header"
        ? {
            method: "POST",
            body: data,
          }
        : {
            method: "POST",
            headers: {
              Accept: "application/json",
              authorization: global.getToken(),
            },
            body: data,
          };

    let apiUrl = version === "v2" ? apiUrlV2 + url : globalUrl + url;
    return fetch(apiUrl, header)
      .then((response) => {
        return response
          .json()
          .then((json) => {
            if (response.status === 401) {
              store.dispatch(logoutSuccess());
            }
            return Promise.resolve({
              responseData: json,
              response: response,
            });
          })
          .catch((err) => {
            return Promise.resolve({
              response: response,
            });
          });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },
  postWithBasicAuth: (url, data, headerData) => {
    let header = {
      method: "POST",
      headers: {
        //add authorization
        authorization: "Basic ",
      },
      body: JSON.stringify(data),
    };
    return fetch(globalUrl + url, header)
      .then((response) => {
        return response
          .json()
          .then((json) => {
            if (response.status === 401) {
              store.dispatch(logoutSuccess());
            }
            return Promise.resolve({
              responseData: json,
              response: response,
            });
          })
          .catch((err) => {
            return Promise.resolve({
              response: response,
            });
          });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },
  put: (url, data, basicAuth) => {
    //set token for basic authorization
    let token = "Basic ";
    let header = data
      ? {
          method: "PUT",
          headers: {
            authorization:
              basicAuth && basicAuth.basicAuth ? token : global.getToken(),
          },
          body: JSON.stringify(data),
        }
      : {
          method: "PUT",
          headers: {
            authorization:
              basicAuth && basicAuth.basicAuth ? token : global.getToken(),
          },
        };
    return fetch(globalUrl + url, header)
      .then((response) => {
        return response
          .json()
          .then((json) => {
            if (response.status === 401) {
              store.dispatch(logoutSuccess());
            }
            return Promise.resolve({
              responseData: json,
              response: response,
            });
          })
          .catch((err) => {
            return Promise.resolve({
              response: response,
            });
          });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },

  get: (url, version) => {
    let apiUrl = version === "v2" ? apiUrlV2 + url : globalUrl + url;
    return fetch(apiUrl, {
      method: "GET",
      headers: {
        authorization: global.getToken(),
      },
    })
      .then((response) => {
        return response
          .json()
          .then((json) => {
            if (response.status === 401) {
              store.dispatch(logoutSuccess());
            }
            return Promise.resolve({
              responseData: json,
              response: response,
            });
          })
          .catch((err) => {
            return Promise.resolve({
              response: response,
            });
          });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },

  getWithBasicHeader: (url) => {
    //set basic authorization token
    let token = "Basic ";
    return fetch(globalUrl + url, {
      method: "GET",
      headers: {
        authorization: token,
      },
    })
      .then((response) => {
        return response
          .json()
          .then((json) => {
            if (response.status === 401) {
              store.dispatch(logoutSuccess());
            }
            return Promise.resolve({
              responseData: json,
              response: response,
            });
          })
          .catch((err) => {
            return Promise.resolve({
              response: response,
            });
          });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },

  delete: (url, data) => {
    return fetch(globalUrl + url, {
      method: "DELETE",
      headers: {
        authorization: global.getToken(),
      },
      ...(data ? { body: JSON.stringify(data) } : ""),
    })
      .then((response) => {
        return response.json().then((json) => {
          if (response.status === 401) {
            store.dispatch(logoutSuccess());
          }
          return Promise.resolve({
            responseData: json,
            response: response,
          });
        });
      })
      .catch((err) => {
        console.log("api post error:", err);
        throw err;
      });
  },

  patch: (url, data) => {
    let header = {
      method: "PATCH",
      headers: {
        authorization: global.getToken(),
      },
      ...(data && { body: JSON.stringify(data) }),
    };
    return fetch(globalUrl + url, header)
      .then((response) => {
        return response.json().then((json) => {
          if (response.status === 401) {
            store.dispatch(logoutSuccess());
          }
          return Promise.resolve({
            responseData: json,
            response: response,
          });
        });
      })
      .catch((err) => {
        console.log("api patch error:", err);
        throw err;
      });
  },
};
